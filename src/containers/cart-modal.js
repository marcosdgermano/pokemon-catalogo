import { connect } from 'react-redux';
import CartModal from '../components/cart-modal';
import { closeModal } from '../redux/actions/modal';

const mapStateToProps = ({ modal }) => ({ visible: modal.visible });

export default connect(mapStateToProps, { closeModal })(CartModal);