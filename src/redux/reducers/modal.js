const initialState = {
  visible: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'OPEN_MODAL':
      return { visible: true };
    case 'CLOSE_MODAL':
      return { visible: false };
    default:
      return state;
  }
};