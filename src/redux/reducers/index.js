import { combineReducers } from 'redux';
import cart from './cart';
import pokemon from './pokemon';
import pokemons from './pokemons';
import modal from './modal';

export default combineReducers({ pokemon, pokemons, cart, modal });