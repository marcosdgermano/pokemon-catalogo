import React from 'react';
import styled from 'styled-components';
import { isMobile } from 'react-device-detect';
import Card from '../containers/card';

class Grid extends React.Component {
  render() {
    const { children = [] } = this.props;

    return (
      <GridWrapper isMobile={isMobile}>
        {children.map(elem => (
          <CardWrapper>
            <Card pokemon={elem} />
          </CardWrapper>
        ))}
      </GridWrapper>
    );
  }
}

const GridWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  ${({ isMobile }) => isMobile && 'justify-content: center;'};
`;

const CardWrapper = styled.div`
  margin: 0 5px 10px;
`;

export default Grid;