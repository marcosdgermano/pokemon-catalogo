import React from 'react';
import { connect } from 'react-redux';
import { isMobile } from 'react-device-detect';
import { ThemeProvider } from 'styled-components';
import { fetchPokemonsBytype } from '../redux/actions/pokemon';
import themes from '../theme-context';
import CatalogPage from './catalog-page';
import CatalogPageMobile from './catalog-page.mobile';

class App extends React.Component {
  componentDidMount() {
    this.props.fetchPokemonsBytype(this.props.brand);
  }

  render () {
    const theme = themes[this.props.brand];

    if(isMobile) 
      return <ThemeProvider theme={theme}><CatalogPageMobile /></ThemeProvider>;

    return <ThemeProvider theme={theme}><CatalogPage /></ThemeProvider>;
  }
};

export default connect(null, { fetchPokemonsBytype })(App);