import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Grid from '../containers/grid';
import Cart from '../containers/cart';

class CatalogPage extends React.Component {
  render () {
    return (
      <Wrapper>
        <GridWrapper>
          <Grid>{this.props.pokemons}</Grid>
        </GridWrapper>
        <CartWrapper>
          <Cart />
        </CartWrapper>
      </Wrapper>
    );
  }
};

const Wrapper = styled.div`
  display: flex;
`;

const GridWrapper = styled.div`
  width: 70%;
  background-color: ${props => props.theme.terciary};
`;

const CartWrapper = styled.div``;

const mapStateToProps = ({pokemons}) => ({ pokemons });

export default connect(mapStateToProps)(CatalogPage);