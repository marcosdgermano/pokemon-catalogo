import React from 'react';
import { connect } from 'react-redux';
import styled, { css } from 'styled-components';
import { ReactSVG } from 'react-svg';
import { openModal } from '../redux/actions/modal';
import {ReactComponent as CartImage} from '../cart-icon.svg';

class CartIcon extends React.Component {
  openModal() {
    this.props.openModal();
  }

  render() {
    const { isCartEmpty } = this.props;

    return (
      <IconWrapper isCartEmpty={isCartEmpty}>
        <CartImageUI onClick={this.openModal.bind(this)} />
      </IconWrapper>
    )
  }
}

const IconWrapper = styled.div`
  display: block;
  position: fixed;
  ${({ isCartEmpty }) => !isCartEmpty && css`
    &::after {
      left: -15%;
      top: 7px;
      position: relative;
      height: 15px;
      width: 15px;
      border-radius: 50%;
      content: '';
      display: inline-block;
      -moz-border-radius: 50%;
      -webkit-border-radius: 50%;
      background-color: ${({ theme }) => theme.secondary};
    }
  `}
  
`;

const CartImageUI = styled(CartImage)`
  width: 24px;
  height: 20px;
  fill: white;
`;

const Icon = styled.svg`
  height: 35px;
  width: 35px;
`;

const mapStateToProps = ({ cart }) => ({ isCartEmpty: cart.length < 1 });

export default connect(mapStateToProps, { openModal })(CartIcon);