import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Grid from '../containers/grid';
import Cart from '../containers/cart';
import CartIcon from './cart-icon';
import { fetchPokemonsBytype } from '../redux/actions/pokemon';

class CatalogPageMobile extends React.Component {
  render() {
    return (
      <>
        <GridWrapper>
          <CartIcon />
          <Grid>{this.props.pokemons}</Grid>
        </GridWrapper>
        <Cart />
      </>
    );
  }
}

const GridWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  background-color: ${props => props.theme.terciary};
  padding: 0 auto;
  z-index: 1;
`;

const mapStateToProps = ({pokemons}) => ({ pokemons });

export default connect(mapStateToProps, { fetchPokemonsBytype })(CatalogPageMobile);