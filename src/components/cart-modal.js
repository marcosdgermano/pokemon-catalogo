import React from 'react';
import { connect } from 'react-redux';
import styled, { css } from 'styled-components';

class ModalMobile extends React.Component {
  hideModal() {
    this.props.closeModal();
  }

  render() {
    const { children, visible } = this.props;
    const xSymbol = '\u2715';

    return(
      <ModalUI visible={visible}>
        <HideButton onClick={this.hideModal.bind(this)}>{xSymbol}</HideButton>
        {children}
      </ModalUI>
    );
  }
}

const HideButton = styled.button`
  align-self: flex-end;
  border-width: 0px;
  height: 35px;
  width: 35px;
`;

const ModalUI = styled.div`
  width: 100%;
  height: 100%;
  position: fixed;
  z-index: 1000;
  top: 0;
  left: 0;
  background-color: ${({ theme }) => theme.secondary};
  display: flex;
  flex-direction: column;
  transform: translate3d(102%, 0px, 0px);
  ${props => props.visible && css`
      transform: translate3d(0px, 0px, 0px);
    `}
`;

const mapStateToProps = ({ modal }) => ({ visible: modal.visible });

export default connect(mapStateToProps)(ModalMobile);